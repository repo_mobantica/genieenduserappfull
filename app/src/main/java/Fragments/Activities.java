package Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.genieiot.gsmarthome.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import Adapter.AdapterActivity;
import Database.DatabaseHandler;
import Session.SessionManager;

import static Database.DatabaseHandler.NOTIFICATION_MSG;
import static Database.DatabaseHandler.NOTIFICATION_READ_FLAG;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.USER_IMAGE;

/**
 * Created by Genie IoT on 9/13/2016.
 */
public class Activities extends Fragment {

    private View view;
    ListView lv;
    Context context;
    DatabaseHandler db;
    private TextView tab_badge;
    SessionManager session;

    ArrayList prgmName;
    public static int[] prgmImages = {R.drawable.image2,R.drawable.image1,R.drawable.image3,R.drawable.image1,R.drawable.image3,R.drawable.image1,R.drawable.physical_icon};
    public static String[] prgmNameList = {"Jason switch ON Bulb", "Martin switch ON Fan ", "Jenifer switch OFF Lamp ", "Martin switch ON AC", "Mary switch OFF Tube","Grace switch ON Washing Machine"};
    public static String[] roomNameList = {"Hall", "Bedroom", "Kitchen", "Hall","Bedroom","Kitchen"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activites, container, false);
        initWidgets();
        return view;
    }
    private void initWidgets() {
        context = getActivity();
        session=new SessionManager(getActivity());
        tab_badge= (TextView) view.findViewById(R.id.tab_badge);
//        tab_badge.setText("3");
        lv = (ListView) view.findViewById(R.id.lstActivity);
        setNotificationAdapter();

    }

    private void setNotificationAdapter() {
        try {
            DatabaseHandler database = new DatabaseHandler(getActivity());
            if (session.getDemoUser().equals("DemoUser")) {
                ArrayList<LinkedHashMap<String, String>> msgList = getDemoUserNotification();
                lv.setAdapter(new AdapterActivity(getActivity(), prgmImages, msgList));
            } else {
                ArrayList<LinkedHashMap<String, String>> msgList = database.getNotificationMessage();
                lv.setAdapter(new AdapterActivity(getActivity(), prgmImages, msgList));
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private ArrayList<LinkedHashMap<String,String>> getDemoUserNotification() {
        ArrayList<LinkedHashMap<String,String>> mMap=new ArrayList<>();

        for(int i=0;i<prgmNameList.length;i++){
            LinkedHashMap<String,String> lMap=new LinkedHashMap<>();
            lMap.put(NOTIFICATION_MSG,prgmNameList[i]);
            lMap.put(NOTIFICATION_READ_FLAG,"1");
            lMap.put(ROOM_NAME,roomNameList[i]);
            lMap.put(USER_IMAGE,"");
            mMap.add(lMap);
        }
        return mMap;
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(messageActivity, new IntentFilter("NotificationSend"));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(messageActivity);
    }

    private BroadcastReceiver messageActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            //setActivityCount();
            setNotificationAdapter();
        }
    };
}
