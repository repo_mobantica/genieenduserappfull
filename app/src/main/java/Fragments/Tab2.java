package Fragments;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.gsmarthome.AddNewTask;
import com.genieiot.gsmarthome.AlarmReceiver;
import com.genieiot.gsmarthome.R;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SCHEDULE_DATETIME;
import static Database.DatabaseHandler.SCHEDULE_ID;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.TIME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 10/12/16.
 */

public class Tab2 extends Fragment implements View.OnClickListener, NumberPicker.OnValueChangeListener{

    View view;
    Context context;
    Button btn_scedule;
    //Overriden method onCreateView
    EditText edittxt_hour,edittxt_min;
    private RadioGroup rgLock,rgSwitch;
    private ProgressDialog pDialog;
    String mSwitchID="",mUserID="";
    String myFormat;
    SimpleDateFormat sdf;
    Date currDate;
    private RadioButton rbSwitchOn,rbSwitchOff,rbLockOn,rbLockOff;
    HashMap<String,String> mapList;
    TextView txtText,txtTurnSwitch;
    private SessionManager session;
    private String switchName,mScheduletime;
    private String mSwitchStatus="";

    String formattedDate,hourTime,minTime;
    private String mRoomName;
    private String messageType="";

    private LinearLayout layoutDimmer;
    private static final String TAG = "Tab2";
    DiscreteSeekBar seekDimmer;
    String dimmerValueSend="0";
    private String dimmerStatus;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.timernew, container, false);
        context=container.getContext();
        session=new SessionManager(getActivity());
        btn_scedule = (Button) view.findViewById(R.id.btn_scedule);
        edittxt_hour = (EditText) view.findViewById(R.id.edittxt_hour);
        edittxt_min = (EditText) view.findViewById(R.id.edittxt_min);

        rgSwitch= (RadioGroup) view.findViewById(R.id.rgSwitch);

        //Dimmer Layout
        layoutDimmer=(LinearLayout)view.findViewById(R.id.layoutDimmer);
        seekDimmer=(DiscreteSeekBar)view.findViewById(R.id.seekDimmer);

        rbSwitchOn= (RadioButton) view.findViewById(R.id.rbSwitchOn);
        rbSwitchOff= (RadioButton) view.findViewById(R.id.rbSwitchOff);
        mapList= (HashMap<String, String>) this.getArguments().getSerializable("SwitchInfo");
        txtText= (TextView) view.findViewById(R.id.txtText);
        txtTurnSwitch= (TextView) view.findViewById(R.id.txtTurnSwitch);
        dimmerStatus=mapList.get(DIMMER_STATUS);

        if(dimmerStatus.equals("0")){
            layoutDimmer.setVisibility(View.GONE);
        }else{
            layoutDimmer.setVisibility(View.VISIBLE);
            try {
                int dimmervalueset=Integer.parseInt(mapList.get(DIMMER_VALUE));
                seekDimmer.setProgress(((dimmervalueset * 100) / 75));
            }catch(Exception e){e.printStackTrace();}
        }

        switchName=mapList.get("SwitchName");
        txtText.setText("What do you want "+mapList.get("SwitchName")+" to do? ");
        mRoomName=mapList.get("RoomName");
       // txtTurnSwitch.setText("Turn"+ " "+switchName);

        btn_scedule.setOnClickListener(this);
        edittxt_hour.setFocusable(false);
        edittxt_min.setFocusable(false);

        edittxt_hour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showHour(24,"Select Hour");

            }
        });

        edittxt_min.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                showMin(59,"Select Minutes");

            }
        });

        mSwitchID=mapList.get(SWITCH_ID);
        mUserID=session.getUSERID();


        return view;
    }
    public void showHour(int maxval, String title) {
         LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.number_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        final NumberPicker np = (NumberPicker) dialoglayout.findViewById(R.id.numberPicker);
        np.setMaxValue(maxval);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        builder.setPositiveButton(Html.fromHtml("<p style=\"color:orange;\">Set</p>"), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        edittxt_hour.setText(String.valueOf(np.getValue()));
//                        builder.dismiss();
                    }
                });
        builder.setNegativeButton(Html.fromHtml("<p style=\"color:orange;\">Cancel</p>"), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();

    }
    public void showMin(int maxval, String title)  {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.number_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        final NumberPicker np = (NumberPicker) dialoglayout.findViewById(R.id.numberPicker);
        np.setMaxValue(maxval);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        builder.setPositiveButton(Html.fromHtml("<p style=\"color:orange;\">Set</p>"), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        edittxt_min.setText(String.valueOf(np.getValue()));
//                        builder.dismiss();
                    }
                });
        builder.setNegativeButton(Html.fromHtml("<p style=\"color:orange;\">Cancel</p>"), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();

    }
    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
    }
    private void setAlarm(Calendar targetCal, String command,int alrmID){

        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("Command", command);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alrmID, intent, 0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(), pendingIntent);

    }
    @Override
    public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.btn_scedule:
                        onClickOfSetTimer();
                    break;
            }
        }
    private void onClickOfSetTimer() {

        String mLockStatus="";
        if(((RadioButton)rgSwitch.findViewById(R.id.rbSwitchOn)).isChecked()){
            mSwitchStatus="1";
        }
        if(((RadioButton)rgSwitch.findViewById(R.id.rbSwitchOff)).isChecked()){
            mSwitchStatus="0";
        }

        if(mSwitchStatus.equals("") && mLockStatus.equals("")){
            Toast.makeText(getActivity(), "Please select one of operation.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(edittxt_hour.getText().toString().equals("")){
            Toast.makeText(context, "Please select hour.", Toast.LENGTH_SHORT).show();
            return;

        }
        if(edittxt_min.getText().toString().equals("")){
            Toast.makeText(context, "Please select min.", Toast.LENGTH_SHORT).show();
            return;
        }
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");


        String hourtime=edittxt_hour.getText().toString();
        String mintime=edittxt_min.getText().toString();

        Calendar now = Calendar.getInstance();
        if(!hourtime.equals("")) {
         now.add(Calendar.HOUR, Integer.parseInt((hourtime)));
        }
        if(!mintime.equals("")) {
            now.add(Calendar.MINUTE, Integer.parseInt((mintime)));
        }

        mScheduletime=df1.format(now.getTime());

        formattedDate = df.format(c.getTime());

        edittxt_hour.setText(edittxt_hour.getText().toString());
        edittxt_min.setText(edittxt_min.getText().toString());

            Date date = null;
            date=now.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            hourTime = sdf.format(date);
        if(session.getDemoUser().equals("DemoUser")){
               setSchedularData();
        }else {
            new AsyncScheduleTask().execute(mSwitchStatus, mLockStatus, mScheduletime);
        }
    }
    private void setSchedularData() {
        HashMap<String,String> mMap=new HashMap<>();
        mMap.put(SWITCH_ID,mSwitchID);
        mMap.put(SWITCH_NAME,switchName);
        mMap.put(SCHEDULE_DATETIME,formattedDate);
        mMap.put(SWITCH_STATUS,mSwitchStatus);
        mMap.put(TIME,hourTime);
        mMap.put(ROOM_NAME,mapList.get("RoomName"));
        mMap.put(IP,mapList.get(IP));
        DatabaseHandler db=new DatabaseHandler(context);
        db.insertSchedulerInfo(mMap);
        Intent alarmIntent = new Intent(getActivity(), AlarmReceiver.class);
        alarmIntent.putExtra(SWITCH_ID,mSwitchID);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() +calulateTimeDiff(mScheduletime),pendingIntent);
        Intent intent=new Intent(getActivity(), AddNewTask.class);
        intent.putExtra("SwitchInfo",mapList);
        startActivity(intent);
    }
    private class AsyncScheduleTask extends AsyncTask<String,Void,String> {
        String dimmerValue;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
            dimmerValue=""+((seekDimmer.getProgress()*75)/100);
        }
        @Override
        protected String doInBackground(String... params) {
            Constants res=new Constants();
            String response=null;
            if(URL_GENIE.equals(URL_GENIE_AWS)){
                messageType=INTERNET;
            }
            else{
                messageType=LOCAL_HUB;
            }
            try{
                JSONObject jReqBody=new JSONObject();
                jReqBody.put("switchId",mSwitchID);
                jReqBody.put("switchStatus",params[0]);
                jReqBody.put("lockStatus",params[1]);
                jReqBody.put("scheduleDateTime",params[2]);
                jReqBody.put("userId",mUserID);
                jReqBody.put("messageFrom",messageType);
                jReqBody.put("dimmerValue",dimmerValue);
                jReqBody.put("dimmerStatus",dimmerStatus);

                Log.d(TAG, "schedule doInBackground: "+jReqBody);

                SessionManager session=new SessionManager(getActivity());
                response=res.doPostRequest(URL_GENIE+"/switch/scheduleswitch",""+jReqBody,session.getSecurityToken());
                Log.d( "SHEDULE_LIST_TIMER: ","JSONOBJECT1===="+jReqBody);
                Log.d( "SHEDULE_LIST_TIMER: ","URL1===="+URL_GENIE_AWS+"/switch/scheduleswitch");
                Log.d( "SHEDULE_LIST_TIMER: ","RESPONSE1===="+response);
            }catch(Exception e){}
            return response;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null){
                pDialog.cancel();
            }
            parseResult(result,dimmerValue);
        }
    }
    private void parseResult(String result,String dimmerValue) {
        if(result!=null){
            try {
                JSONObject jObj = new JSONObject(result);
                if(jObj.getString("status").equals("SUCCESS")){

                    if(messageType.equals(INTERNET)){
                        Intent intent=new Intent(getActivity(), AddNewTask.class);
                        intent.putExtra("SwitchInfo",mapList);
                        intent.putExtra("Operation","ADD");
                        startActivity(intent);
                        getActivity().finish();
                    }

                    JSONObject jResult=new JSONObject(jObj.getString("result"));
                     DatabaseHandler db=new DatabaseHandler(context);
                     HashMap<String,String> mMap=new HashMap<>();
                     mMap.put(SWITCH_ID,mSwitchID);
                     mMap.put(SWITCH_NAME,switchName);
                     mMap.put(SCHEDULE_DATETIME,formattedDate);
                     mMap.put(SWITCH_STATUS,mSwitchStatus);
                     mMap.put(TIME,hourTime);
                     mMap.put(ROOM_NAME,mRoomName);

                     mMap.put(SCHEDULE_ID,jResult.getString("scheduleSwitchId"));
                     mMap.put(DIMMER_VALUE,dimmerValue);
                     mMap.put(DIMMER_STATUS,dimmerStatus);

                     db.insertSchedulerInfo(mMap);
                    Toast.makeText(context,getResources().getString(R.string.SCHEDULE_CREATE), Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(getActivity(), AddNewTask.class);
                    intent.putExtra("SwitchInfo",mapList);
                    intent.putExtra("Operation","ADD");
                    startActivity(intent);
                    getActivity().finish();

                }else{

                    Toast.makeText(context, jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){}


        }else{
            Toast.makeText(context,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
        }
    }


    private long calulateTimeDiff(String startDate) {
        Date d1 = null;
        Date d2 = null;
        long diff=0;
        Calendar calander = Calendar.getInstance();
        SimpleDateFormat formatS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//MM/dd/yyyy HH:mm:ss
        String stopDate= formatS.format(calander.getTime());
        try{

            d1 = formatS.parse(startDate);
            d2 = formatS.parse(stopDate);

            Log.d("Start Time",startDate);
            Log.d("Stop Time",stopDate);
            //in milliseconds
            diff = d1.getTime() - d2.getTime();
            Log.d("Diff...",diff+"");

        }catch(Exception e){}
        return diff;
    }
}




