package Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.gsmarthome.AddNewTask;
import com.genieiot.gsmarthome.LivingRoom;
import com.genieiot.gsmarthome.R;
import com.genieiot.gsmarthome.Schedular_Activity;
import com.genieiot.gsmarthome.SwitchSelectionActivity;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import Adapter.ProfileAdapter;
import Database.DatabaseHandler;
import Session.Constants;
import Session.IOnClickListner;
import Session.IOnLongClickListener;
import Session.MqttlConnection;
import Session.NetworkConnectionInfo;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.ISMODE_SELECTED;
import static Database.DatabaseHandler.ISSHORCUT;
import static Database.DatabaseHandler.MODE_ID;
import static Database.DatabaseHandler.MODE_NAME;
import static Database.DatabaseHandler.OFF_MODE;
import static Database.DatabaseHandler.ON_MODE;
import static Database.DatabaseHandler.PANEL_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.NOTINTERNET;
import static Session.Constants.TOPIC_INTERNET;
import static Session.Constants.TOPIC_LOCAL;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 17/3/17.
 */

public class ProfileFragment extends Fragment implements IOnClickListner, IOnLongClickListener {
    private View rootView;
    private RecyclerView recycler_profile;
    private ProfileAdapter adapter;
    private DatabaseHandler db;
    private ProgressDialog pDialog;
    private static final String TAG = "ProfileFragment";ArrayList<HashMap<String,String>> arrListProfile=new ArrayList<>();
    String broker = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    MqttClient mqqtClient = null;
    MemoryPersistence persistence;
    String topic ;
    String type;
    private SessionManager sessionManager;
    String connectionValue;
    MqttlConnection mqttlConnection;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.profile_fragment_layout, container, false);
        recycler_profile=(RecyclerView)rootView.findViewById(R.id.recycler_profile);

        db=new DatabaseHandler(getActivity());
        mqttlConnection=new MqttlConnection();


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_profile.setLayoutManager(mLayoutManager);

        setAdapter();

        return rootView;
    }

    private void setAdapter() {


        arrListProfile=db.getAllModeInfo();
        HashMap<String,String> map1=new HashMap<>();
        map1.put(MODE_NAME,"Add New Mode");
        map1.put(ISMODE_SELECTED,"0");
        arrListProfile.add(map1);
        Log.d("testing_all_data","==="+arrListProfile);
        adapter=new ProfileAdapter(getActivity(),arrListProfile,ProfileFragment.this);
        recycler_profile.setAdapter(adapter);

    }

    @Override
    public void onClickListner(HashMap<String, String> map) {

        if(map.get(MODE_NAME).equals("Add New Mode")) {
            showFolderNameDialog("NEW","");
        }else{
            if((map.get(ON_MODE)==null || map.get(ON_MODE).isEmpty()) && (map.get(OFF_MODE)==null || map.get(OFF_MODE).isEmpty())){

                Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
                intentON.putExtra("ACTION","00");
                intentON.putExtra("ID",map.get(MODE_ID));
                startActivityForResult(intentON, 2);

            }else{
                //new AsyncModeActivationTask().execute(map.get(MODE_ID));
            }
        }
    }


    private void showFolderNameDialog(final String opt, final String id) {
        final Dialog dialogDeviceID = new Dialog(getActivity());
        // Include dialog.xml file
        dialogDeviceID.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDeviceID.setContentView(R.layout.dailog_name);
        TextView txtOk = (TextView) dialogDeviceID.findViewById(R.id.txtOk);
        TextView tvTitle = (TextView) dialogDeviceID.findViewById(R.id.tvTitle);
        final EditText etName=(EditText)dialogDeviceID.findViewById(R.id.etName);
        final TextView txtCancel = (TextView) dialogDeviceID.findViewById(R.id.txtCancel);

        tvTitle.setText("Mode Name ");
        if(opt.equals("NEW")) {
            txtOk.setText("CREATE");
        }else{
            etName.setText(opt);
            txtOk.setText("RENAME");
        }

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDeviceID.dismiss();
                String name=etName.getText().toString();
                if(!name.equals("")) {
                    dialogDeviceID.dismiss();
                    if(opt.equals("NEW")) {
                        db.createMode(name);
                    }else{
                        db.updateModeName(id,name);
                    }
                    setAdapter();
                }else{
                    Toast.makeText(getActivity(), "Please enter name", Toast.LENGTH_SHORT).show();
                }
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDeviceID.dismiss();
            }
        });
        dialogDeviceID.show();

    }

    @Override
    public void OnLongClickListener(final HashMap<String, String> mMap) {
        String shorcut;
        new AsyncTaskCheckRouter().execute();
        if(mMap.get(ISSHORCUT).equals("0")) {
            shorcut= "Add to Shortcut";
        }else{
            shorcut="Remove to Shortcut";
        }

        final String finalShorcut=shorcut;
        final CharSequence[] strArray = {"Edit","Delete","Rename",shorcut,"Turn on all","Turn off all"};
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setItems(strArray,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                 editSelectedSwitchList(mMap);
                                break;
                            case 1:
                                alertDialogDelete(mMap);
                                break;
                            case 2:
                                renameModeName(mMap);
                                break;
                            case 3:
                                addOrRemoveShorcut(mMap,finalShorcut);
                                break;
                            case 4:
                                chkStatus();
                                new AsyncModeActivationTask().execute(mMap.get(MODE_ID),"1",connectionValue);

                                Toast.makeText(getContext(), "Turn On all", Toast.LENGTH_SHORT).show();
                            break;
                            case 5:
                                chkStatus();
                                new AsyncModeActivationTask().execute(mMap.get(MODE_ID),"0",connectionValue);
                                Toast.makeText(getContext(), "Turn Off all", Toast.LENGTH_SHORT).show();
                                break;

                        }
                    }
                });

        alertDialogBuilder.show();

    }

    private void addOrRemoveShorcut(HashMap<String, String> mMap, String shorcut) {
        String message=db.updateModeShorcutFlag(mMap,shorcut.equals("Add to Shortcut")?"1":"0");
        Toast.makeText(getActivity(),message, Toast.LENGTH_LONG).show();
        setAdapter();
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("refreshShortcut").putExtra("KEY","1"));
    }

    private void editSelectedSwitchList(HashMap<String, String> mMap) {
        Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
        intentON.putExtra("ACTION","00");
        intentON.putExtra("ID",mMap.get(MODE_ID));
        intentON.putExtra("EDIT",mMap.get(ON_MODE));
        startActivityForResult(intentON, 2);
    }

    private void renameModeName(HashMap<String, String> mMap) {
        showFolderNameDialog(mMap.get(MODE_NAME),mMap.get(MODE_ID));
    }

    private void alertDialogDelete(final HashMap<String,String> mMap)  {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete);

        TextView txtCancel= (TextView) dialog.findViewById(R.id.txtCancel);
        TextView tvTitle= (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvMessage= (TextView) dialog.findViewById(R.id.tvMessage);
        TextView txtDelete= (TextView) dialog.findViewById(R.id.txtDelete);
        tvTitle.setText("Delete Mode ");
        //tvMessage.setText("Delete Mode ");

        txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new AddNewTask.AsyncDeleteScheduleTask().execute(mMap);
                //Toast.makeText(getActivity(),mMap.get(MODE_ID),Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                db.deleteMode(mMap.get(MODE_ID));
                setAdapter();
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("refreshShortcut").putExtra("KEY","1"));
                Toast.makeText(getActivity(),mMap.get(MODE_NAME)+" is deleted successfully",Toast.LENGTH_LONG).show();

            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class AsyncModeActivationTask extends AsyncTask<String,Void,String> {
        HashMap<String,String> modeData;
        String messageType="";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Completing your wish...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants req=new Constants();
            String response=null;
            ArrayList<String> switchnumber = new ArrayList<>();
            ArrayList<String> panelName=new ArrayList<>();
            ArrayList<String> dimmerStatus=new ArrayList<>();
            SessionManager sessionManager=new SessionManager(getActivity());
            String mqttMessage=null;

            modeData=db.getModeInfoByID(params[0]);
            String onoffstatus=params[1];
            String connectionValue1=params[2];
            int i;
            Log.d("testing_get_switch","all data===="+modeData);
            try {

                String mONData=modeData.get(ON_MODE);
                String mPanelNumber=modeData.get(PANEL_NAME);
                String mDimmerStatus=modeData.get(DIMMER_STATUS);


                String[] arrOfStr = mONData.split(":");
                for ( String s : arrOfStr )
                {
                    switchnumber.add(s);
                }

                String[] arrOfStr1 = mPanelNumber.split(":");
                for ( String s : arrOfStr1 )
                {
                    panelName.add(s);
                }

                String[] arrOfStr2 = mDimmerStatus.split(":");
                for ( String s : arrOfStr2 )
                {
                    dimmerStatus.add(s);
                }


                for( i=0;i<=switchnumber.size();i++)
                {

                    if(connectionValue1 == LOCAL_HUB)
                    {
                        if(onoffstatus.equals("1"))
                        {
                            if(dimmerStatus.get(i).equals("1"))
                            {
                                mqttMessage = sessionManager.getUSERID()+"/"+switchnumber.get(i)+"/"+onoffstatus+"/"+"50"+"/"+connectionValue1+"/"+panelName.get(i);
                                Log.d("publish_switch_all12","on off msg===="+mqttMessage);
                            }
                            else
                            {
                                mqttMessage = sessionManager.getUSERID()+"/"+switchnumber.get(i)+"/"+onoffstatus+"/"+"0"+"/"+connectionValue1+"/"+panelName.get(i);
                                Log.d("publish_switch_all12","on off msg===="+mqttMessage);
                            }
                        }
                        else
                        {
                            mqttMessage = sessionManager.getUSERID()+"/"+switchnumber.get(i)+"/"+onoffstatus+"/"+"0"+"/"+connectionValue1+"/"+panelName.get(i);
                            Log.d("publish_switch_all12","on off msg===="+mqttMessage);
                        }
                    }
                    else
                    {

                        if(onoffstatus.equals("1"))
                        {
                            if(dimmerStatus.get(i).equals("1"))
                            {
                                mqttMessage = sessionManager.getUSERID()+"/"+switchnumber.get(i)+"/"+onoffstatus+"/"+"50"+"/"+connectionValue1+"/"+panelName.get(i)+"/"+sessionManager.getHomeId();
                                Log.d("publish_switch_all12","on off msg===="+mqttMessage);
                            }
                            else
                            {
                                mqttMessage = sessionManager.getUSERID()+"/"+switchnumber.get(i)+"/"+onoffstatus+"/"+"0"+"/"+connectionValue1+"/"+panelName.get(i)+"/"+sessionManager.getHomeId();
                                Log.d("publish_switch_all12","on off msg===="+mqttMessage);
                            }
                        }
                        else
                        {
                            mqttMessage = sessionManager.getUSERID()+"/"+switchnumber.get(i)+"/"+onoffstatus+"/"+"0"+"/"+connectionValue1+"/"+panelName.get(i)+"/"+sessionManager.getHomeId();
                            Log.d("publish_switch_all12","on off msg===="+mqttMessage);
                        }

                    }

                    //    db.updateSwitchStatus(map.get(SWITCH_ID), map.get(SWITCH_STATUS), map.get(DIMMER_VALUE));



                if(connectionValue1 == LOCAL_HUB)
                {
                  /*  setMqttClient();
                    mqttMessage= "wish/Local_Switch_On_Off/"+mqttMessage;
                    MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());

                    message2.setQos(0);


                    getMqttConnection().publish(topic, message2);
                    Log.d("publish_switch_all12","on off msg===="+message2);*/

                    mqttMessage= "wish/Local_Switch_On_Off/"+mqttMessage;
                    Log.d("switch_ON_OFF_ALL=","on off msg===="+mqttMessage);

                    MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());

                    message2.setQos(0);
                    try {
                        if (mqqtClient != null) {

                            Log.d("publish_switch_all12","on off msg===="+message2);
                            mqttlConnection.getMqttConnectionLocal().publish(TOPIC_LOCAL+sessionManager.getHomeId(),message2);

                        }
                        else
                        {
                            Log.d("publish_switch_all12","on off msg===="+message2);
                            mqttlConnection.getMqttConnectionLocal().publish(TOPIC_LOCAL+sessionManager.getHomeId(),message2);
                            Log.d("publish_switch_all12","on off msg===="+message2);
                        }

                    }catch(Exception e){
                        e.printStackTrace();
                    }



                }
                else {

                    mqttMessage= "wish/Internet_Switch_On_Off/"+mqttMessage;
                    Log.d("switch_ON_OFF_ALL=","on off msg===="+mqttMessage);

                    MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());

                    message2.setQos(0);
                    try {
                        if (mqqtClient != null) {

                            Log.d("publish_switch_all12","on off msg===="+message2);
                            // getMqttConnection().publish(topic, message2);
                            mqttlConnection.getMqttConnectionInternet().publish(TOPIC_INTERNET,message2);
                        }
                        else
                        {
                            Log.d("publish_switch_all12","on off msg===="+message2);
                          //  setMqttClient();
                            // getMqttConnection().publish(topic, message2);
                            mqttlConnection.getMqttConnectionInternet().publish(TOPIC_INTERNET,message2);
                            Log.d("publish_switch_all12","on off msg===="+message2);
                        }

                    }catch(Exception e){
                        e.printStackTrace();
                    }

                }

                }



                db.updateSwitchStatus(switchnumber.get(i), onoffstatus, "50");
                db.updateAllActiveFlag();
                db.updateActiveFlag(modeData.get(MODE_ID));


            }catch (Exception e){}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(pDialog!=null && pDialog.isShowing()){
                pDialog.dismiss();
            }



            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("NotificationSend1"));
/*
            if(result!=null){
                Log.d(TAG, "onPostExecute: "+result);


                if(messageType.equals(INTERNET)){
                    Toast.makeText(getActivity(),"Your wish is completed.",Toast.LENGTH_SHORT).show();
                    db.updateAllActiveFlag();
                    db.updateActiveFlag(modeData.get(MODE_ID));
                    setAdapter();
                }

                try {
                    JSONObject jResult = new JSONObject(result);
                    if (jResult.getString("status").equals("SUCCESS")) {
                        JSONArray jMain=new JSONArray(jResult.getString("result"));

                        for(int i=0;i<jMain.length();i++) {
                            JSONObject jArr=jMain.getJSONObject(i);
                            if(!jArr.getString("status").isEmpty()) {
                                db.updateAllSwitchStatus(jArr.getString("siwtchid"), jArr.getString("status"));
                            }else{
                                db.updateAllSwitchStatus(jArr.getString("profileSwitchid"), jArr.getString("profileStatus"));
                            }
                        }

                        Toast.makeText(getActivity(),"Your wish is completed.",Toast.LENGTH_SHORT).show();
                        db.updateAllActiveFlag();
                        db.updateActiveFlag(modeData.get(MODE_ID));
                        setAdapter();
                    }else{
                        Toast.makeText(getActivity(),getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    Toast.makeText(getContext(), "error"+e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                Log.d("Local",result);
            }else{
                Toast.makeText(getActivity(),getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
            }*/
        }
    }





















    /*   private class AsyncModeActivationTask extends AsyncTask<String,Void,String> {
        HashMap<String,String> modeData;
        String messageType="";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Completing your wish...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants req=new Constants();
            String response=null;
            SessionManager sessionManager=new SessionManager(getActivity());

            modeData=db.getModeInfoByID(params[0]);
            Log.d("Profile_Mode","all data===="+modeData);
            try {
                JSONObject jObj=new JSONObject();
                if(URL_GENIE.equals(URL_GENIE_AWS)){
                    messageType=INTERNET;
                }
                else{
                    messageType=LOCAL_HUB;
                }

                jObj.put("userId",sessionManager.getUSERID());
                String mOFFData=modeData.get(OFF_MODE);
                String mONData=modeData.get(ON_MODE);

//                if(mONData.isEmpty()){
//                    jObj.put("onoffstatus","0");
//                    jObj.put("switchId",modeData.get(OFF_MODE));
//                    jObj.put("profileSwitchId","NA");
//                    jObj.put("profileOnOffStatus","NA");
//                }else {
                    jObj.put("onoffstatus", "1");
                    jObj.put("switchId",mONData);
                    jObj.put("profileSwitchId", mOFFData);
                    jObj.put("profileOnOffStatus","0");
//                }
                jObj.put("messageFrom", messageType);
                jObj.put("modeName",modeData.get(MODE_NAME));
                Log.d("All Switches-->",jObj+messageType);


                Log.d("Profile_Mode","one ===="+jObj);
                response=req.doPostRequest(URL_GENIE + "/switch/turnOnOffAll", jObj+"", sessionManager.getSecurityToken());

            }catch (Exception e){}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(pDialog!=null && pDialog.isShowing()){
                pDialog.dismiss();
            }

            if(result!=null){
                Log.d(TAG, "onPostExecute: "+result);


                if(messageType.equals(INTERNET)){
                    Toast.makeText(getActivity(),"Your wish is completed.",Toast.LENGTH_SHORT).show();
                    db.updateAllActiveFlag();
                    db.updateActiveFlag(modeData.get(MODE_ID));
                    setAdapter();
                }

                try {
                    JSONObject jResult = new JSONObject(result);
                    if (jResult.getString("status").equals("SUCCESS")) {
                        JSONArray jMain=new JSONArray(jResult.getString("result"));

                        for(int i=0;i<jMain.length();i++) {
                            JSONObject jArr=jMain.getJSONObject(i);
                            if(!jArr.getString("status").isEmpty()) {
                                db.updateAllSwitchStatus(jArr.getString("siwtchid"), jArr.getString("status"));
                            }else{
                                db.updateAllSwitchStatus(jArr.getString("profileSwitchid"), jArr.getString("profileStatus"));
                            }
                        }

                        Toast.makeText(getActivity(),"Your wish is completed.",Toast.LENGTH_SHORT).show();
                        db.updateAllActiveFlag();
                        db.updateActiveFlag(modeData.get(MODE_ID));
                        setAdapter();
                    }else{
                        Toast.makeText(getActivity(),getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    Toast.makeText(getContext(), "error"+e.getMessage(), Toast.LENGTH_SHORT).show();
                }

              Log.d("Local",result);
            }else{
                Toast.makeText(getActivity(),getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
            }
        }
    }
*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is
        if(requestCode==2)
        {
            setAdapter();
        }
    }
    private void setMqttClient(){
        try {

       /*     broker = "tcp://"+sessionManager.getRouterIP()+":1883";
            Constants.URL_GENIE = "http://" + sessionManager.getRouterIP() + ":8080/smart_home_local";
            messageType=LOCAL_HUB;
            //  topic="refreshIntGenieHomeId_1";
            broker = "tcp://" + sessionManager.getRouterIP() + ":1883";*/


            broker = "tcp://192.168.0.119:1883";
            topic="refreshIntGenieHomeId_"+sessionManager.getHomeId();
            clientId=System.currentTimeMillis()+"";
            persistence = new MemoryPersistence();
            mqqtClient = new MqttClient(broker, clientId, persistence);

            Log.d("client_id1","broker=="+broker);
            Log.d("client_id1","clientId=="+clientId);
            Log.d("client_id1","persistence=="+persistence);
            Log.d("client_id1","topic=="+topic);

            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);


            Log.d("client_id1","connecting...");
            mqqtClient.connect(connOpts);
            Log.d("client_id1","connected");
          //  mqqtClient.setCallback(Rooms.this);

            mqqtClient.subscribe(topic);
            Log.d("client_id1","published");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception -->",e.toString());
            Log.d("client_id1","mqtt connection error"+e.toString());
        }
    }
    protected MqttClient getMqttConnection() throws MqttException {
        if (mqqtClient != null) {
            System.out.println("reusing connection...");
            return mqqtClient;
        } else {
            Log.d("check_mqtt","Switch clicked inside1.2");
            setMqttClient();
            return mqqtClient;
        }
    }




    public String  chkStatus() {
        final ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting ())
        {
            try {
                new AsyncTaskCheckRouter().execute().get();
            } catch (InterruptedException e) {
                Log.d("connection_checking","checked"+e.getMessage());
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d("connection_checking","checked"+e.getMessage());
                e.printStackTrace();
            }

            return  connectionValue;
        }
        else if (mobile.isConnectedOrConnecting ()) {

            connectionValue=INTERNET;
            return  connectionValue;
        } else {
            connectionValue=NOTINTERNET;

            return  connectionValue;
        }

    }

    private class AsyncTaskCheckRouter extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {

            InetAddress routername = null;

            Log.d("mqtt_checking","checked");
            try {
                routername = InetAddress.getByName("192.168.0.119");
                if (routername.isReachable(300)) //5000
                {
                    Log.d("network_checking12","Router is   reachable");
                    connectionValue = LOCAL_HUB;
                }
                else
                {
                    Log.d("network_checking12","Router is not  reachable");
                    connectionValue = INTERNET;
                }

            } catch (java.io.IOException e) {
                Log.d("network_checking12","Router EXCEPTION"+e.getMessage());
                e.printStackTrace();
            }

            return connectionValue;

        }


    }

}
