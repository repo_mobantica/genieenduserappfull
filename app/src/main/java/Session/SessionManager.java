package Session;

import android.content.Context;
import android.content.SharedPreferences;

import static Database.DatabaseHandler.USER_IMAGE;


/**
 * Created by root on 9/21/16.
 */

public class SessionManager {

    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    public static String X_AUTH_TOKEN = "X-AUTH-TOKEN";
    public static String SECURITY_TOKEN = "Security Token";
    public static String DEVICE_TOKEN = "Device Token";
    public static String FIRST_NAME = "First Name";
    public static String LAST_NAME = "Last Name";
    public static String EMAIL = "Email";
    public static String PHONE = "Phone";
    public static String USERID = "UsetId";
    public static String USER_TYPE = "User Type";
    public static String PASSWORD = "Password";
    public static String APPURL = "AppUrl";
    public static String APPURL_FLAG = "AppUrlFlag";
    public static String SYNC_FLAG = "SyncFlag";
    public static String IP_FIRST = "firstip";

    public static String IP_SECOND = "secondip";
    public static String IP_THIRD = "thirdip";
    public static String ROUTERIP = "routerip";
    public static String TOPIC_NAME = "topic_name";
    public static String SELECTED_SWITCH_FLAG = "selected_switch";


    private static final String IMAGE = "image";
    private static final String SSID = "ssid";
    private static final String SPASSWORD = "spassword";
    private static final String DEMO_USER = "demo_user";
    private static  String HOME_ID = "home_id";

    private static  String COUNT_VAL = "count_val";
    private static  String COUNT_VAL_SWITCH = "count_val";

    public static String getCountValSwitch() {
        return COUNT_VAL_SWITCH;
    }

    public static void setCountValSwitch(String countValSwitch) {
        COUNT_VAL_SWITCH = countValSwitch;
    }




    public static String getCountVal() {
        return COUNT_VAL;
    }

    public static void setCountVal(String countVal) {
        COUNT_VAL = countVal;
    }






    public SharedPreferences getPref() {
        return pref;
    }

    public void setPref(SharedPreferences pref) {
        this.pref = pref;
    }

    public void setEditor(SharedPreferences.Editor editor) {
        this.editor = editor;
    }




    Boolean flag;
//    public static String SECURITY_TOKEN = "Security Token";
//    public static String SECURITY_TOKEN = "Security Token";
//    public static String SECURITY_TOKEN = "Security Token";

    // Context
    Context _context;


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(Constants.PREF_NAME, Constants.PRIVATE_MODE);
        editor = pref.edit();
    }


    public void saveUser(String userId, String firstName, String lastName, String email, String phone, String userType) {
        // Storing login value as TRUE
        editor.putString(USERID, userId);
        editor.putString(FIRST_NAME, firstName);
        editor.putString(LAST_NAME, lastName);
        editor.putString(EMAIL, email);
        editor.putString(PHONE, phone);
        editor.putString(USER_TYPE, userType);
//        editor.putString(IMAGE, image);
        editor.commit();
    }
    public void saveUserImage(String imageUrl) {

        editor.putString(USER_IMAGE, imageUrl);
        editor.commit();
    }

    public String getUserType(){
        String userType = pref.getString(USER_TYPE, "");
        return userType;
    }

    public  Boolean setLockStatus(boolean b)
    {
        editor.putBoolean("true",true);
        editor.commit();
        return true;
    }

    public void setAppUrl(String appUrl){
        editor.putString(APPURL,appUrl);
        editor.commit();
    }

    public String getAPPURL(){
        String url = pref.getString(APPURL,"");
        return url;
    }

    public void setDemoUser(String user){
        editor.putString(DEMO_USER,user);
        editor.commit();
    }

    public String getDemoUser(){
        String url = pref.getString(DEMO_USER,"");
        return url;
    }

    public void setAppUrlFlag(boolean appUrlFlag){
        editor.putBoolean(APPURL_FLAG,appUrlFlag);
        editor.commit();
    }

    public boolean getAppUrlFlag(){
        boolean url = pref.getBoolean(APPURL_FLAG,false);
        return url;
    }

    public  Boolean setUnLockStatus(boolean b)
    {
        editor.putBoolean("false",false);
        editor.commit();
        return true;
    }

    public Boolean getLockStatus()
    {
        Boolean flag=pref.getBoolean("true", Boolean.parseBoolean(null));
        return flag;
    }

    public Boolean getUnLockStatus()
    {
        Boolean flag=pref.getBoolean("true", Boolean.parseBoolean(null));
        return flag;
    }

    public  Boolean setUserSync(boolean b)
    {
        editor.putBoolean(SYNC_FLAG,b);
        editor.commit();
        return true;
    }

    public Boolean getUserSync()
    {
        Boolean flag=pref.getBoolean(SYNC_FLAG,false);
        return flag;
    }

    public  Boolean setSelectedSwitch(boolean b)
    {
        editor.putBoolean(SELECTED_SWITCH_FLAG,b);
        editor.commit();
        return true;
    }

    public Boolean getSelectedSwitch()
    {
        Boolean flag=pref.getBoolean(SELECTED_SWITCH_FLAG,false);
        return flag;
    }


    public void saveSecurityToken(String strToken) {

        editor.putString(SECURITY_TOKEN, strToken);
        editor.commit();
    }

    public void savePassword(String password){
        editor.putString(PASSWORD, password);
        editor.commit();
    }
    public void saveDeviceToken(String mDeviceToken){
        editor.putString(DEVICE_TOKEN, mDeviceToken);
        editor.commit();
    }

    public String getDeviceToken(){
        return pref.getString(DEVICE_TOKEN,"");
    }

    public String getPassword(){
       return pref.getString(PASSWORD,"");
    }


    public String getUSERID() {

        String name = pref.getString(USERID, "");
        return name;
    }

    public String getFirstName() {
        String name = pref.getString(FIRST_NAME, "");
        return name;
    }

    public String getLastName() {

        String name = pref.getString(LAST_NAME, "");

        return name;
    }

    public String getEmail() {

        String name = pref.getString(EMAIL, "");

        return name;
    }

    public String getPhone() {

        String name = pref.getString(PHONE, "");

        return name;
    }

    public String getSecurityToken() {

        String name = pref.getString(SECURITY_TOKEN, "");

        return name;
    }

    public void setUserImage(String imageurl){
        editor.putString(USER_IMAGE,imageurl);
        editor.commit();
    }

    public String getUserImage() {
        String name = pref.getString(USER_IMAGE, "");
        return name;
    }

    public void saveSSID(String ssid){
        editor.putString(SSID, ssid);
        editor.commit();
    }

    public void saveSPassword(String spassword){
        editor.putString(SPASSWORD, spassword);
        editor.commit();
    }

    public String getSSID() {
        String name = pref.getString(SSID, "");
        return name;
    }

    public String getSPassword() {
        String name = pref.getString(SPASSWORD, "");
        return name;
    }

    public void setFirstString(String str){
        editor.putString(IP_FIRST,str);
        editor.commit();
    }

    public String getFirstString() {
        String name = pref.getString(IP_FIRST, "");
        return name;
    }

    public void setSecondString(String str){
        editor.putString(IP_SECOND,str);
        editor.commit();
    }

    public String getSecondString() {
        String name = pref.getString(IP_SECOND, "");
        return name;
    }

    public String getThirdString() {
        String name = pref.getString(IP_THIRD, "");
        return name;
    }

    public void setThirdString(String str){
        editor.putString(IP_THIRD,str);
        editor.commit();
    }

    public String getRouterIP() {
        String name = pref.getString(ROUTERIP, "");
        return name;
    }

    public void setRouterIP(String str){
        editor.putString(ROUTERIP,str);
        editor.commit();
    }

    public String getTopicName() {
        String name = pref.getString(TOPIC_NAME, "");
        return name;
    }

    public void setTopicName(String str){
        editor.putString(TOPIC_NAME,str);
        editor.commit();
    }


    public void setFirstName(String name){
        editor.putString(FIRST_NAME,name);
        editor.commit();
    }

    public void setLastName(String lastName){
        editor.putString(LAST_NAME,lastName);
        editor.commit();
    }


    public String getHomeId() {
        String name = pref.getString(HOME_ID, "");
        return name;
    }

    public void setHomeId(String str){
        editor.putString(HOME_ID,str);
        editor.commit();
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

}

