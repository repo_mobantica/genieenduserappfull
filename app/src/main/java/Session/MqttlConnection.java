package Session;


import android.util.Log;

import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import static Session.Constants.INTERNET;

public class MqttlConnection
{

     SessionManager sessionManager = null;
     String localClient = "119";
     String localInternet = "119cdcd";
     MqttClient mqttConnectionLocal = null;
     MqttClient mqttConnectionInternet = null;
     String localBroker = "tcp://192.168.0.119:1883";
     String internetBroker = "tcp://192.168.0.2:1883";
     MqttlConnection currObj = null;

    MqttClient mqqtClient = null;
    String broker = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    String topic;

    MemoryPersistence persistence;

    public MqttClient getMqttConnectionLocal() throws MqttException {
        if (mqqtClient != null) {
            System.out.println("reusing connection...");
            mqttConnectionLocal();
            return mqqtClient;
        } else {
            Log.d("check_mqtt","Switch clicked inside1.2");
            mqttConnectionLocal();
            return mqqtClient;
        }



    }

    public MqttClient getMqttConnectionInternet() throws MqttException {
        if (mqqtClient != null) {
            System.out.println("reusing connection...");
            mqttConnectionInternet();
            return mqqtClient;
        } else {
            Log.d("check_mqtt","Switch clicked inside1.2");
            mqttConnectionInternet();
            return mqqtClient;
        }



    }


    public MqttClient mqttConnectionLocal()
    {
        try {

            broker= "tcp://192.168.0.119:1883";
           // topic="refreshIntGenieHomeId_"+sessionManager.getHomeId();

            clientId=System.currentTimeMillis()+"";
            persistence = new MemoryPersistence();
            mqqtClient = new MqttClient(broker, clientId, persistence);

            Log.d("client_id151","broker=="+broker);
            Log.d("client_id151","clientId=="+clientId);
            Log.d("client_id151","persistence=="+persistence);
            Log.d("client_id151","topic=="+topic);

            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);


            Log.d("client_id151","connecting...");
            mqqtClient.connect(connOpts);
            Log.d("client_id151","connected");
            mqqtClient.setCallback((MqttCallback) this);

            mqqtClient.subscribe(topic);
            Log.d("client_id151","published");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception -->",e.toString());
            Log.d("client_id1","mqtt connection error"+e.toString());
        }

        return mqqtClient;
    }

    public MqttClient mqttConnectionInternet()
    {
        try {


            topic="refreshIntGenieHome";

            broker = "tcp://103.12.211.52:1883";

            clientId=System.currentTimeMillis()+"";
            persistence = new MemoryPersistence();
            mqqtClient = new MqttClient(broker, clientId, persistence);

            Log.d("client_id151","broker=="+broker);
            Log.d("client_id151","clientId=="+clientId);
            Log.d("client_id151","persistence=="+persistence);
            Log.d("client_id151","topic=="+topic);

            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);


            Log.d("client_id151","connecting...");
            mqqtClient.connect(connOpts);
            Log.d("client_id151","connected");
            mqqtClient.setCallback((MqttCallback) this);

            mqqtClient.subscribe(topic);
            Log.d("client_id151","published");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception -->",e.toString());
            Log.d("client_id1","mqtt connection error"+e.toString());
        }

        return mqqtClient;
    }


}
