package Session;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;

import com.genieiot.gsmarthome.LivingRoom;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by root on 9/21/16.
 */

public class Constants {


    String topic ;
    String topicIn;
    String broker = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    MqttClient mqqtClient = null;
    MemoryPersistence persistence;
    // Shared pref mode
    public static int PRIVATE_MODE = 0;

    public static final String LOCAL_HUB = "Localhub";
    public static final String INTERNET = "Internet";
    public static final String NOTINTERNET = "NotInternet";

    // Sharedpref file name
    public static final String PREF_NAME = "SmartHome";

    // All Shared Preferences Keys
    public static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    public static final String CHANGEIP_PASS = "g1234";

    public static final String MESSAGE_INTERNET_CONNECTION = "Check internet connection!!!";
    public static final String MESSAGE_TRY_AGAIN = "try again!!!";
    //public static final String URL_GENIE = "http://192.168.0.124:9095/"; //local url

    public static final int PORT = 8080;

    //public static final String URL_GENIE = "http://192.168.0.150:8080/gs-accessing-data-jpa-0.1.0/"; //local url
    //public static final String URL_GENIE = "http://Default-Environment.dbrqfrnqhm.us-west-2.elasticbeanstalk.com/"; //Amazon url
    //public static final String URL_GENIE = "http://192.168.0.119:8080/Final_GSmart1/"; //Raspberry


    // public static String URL_GENIE_AWS = "http://backend.genieiot.com/"; //Raspberry
    // public static String URL_GENIE_AWS = "http://preprod.genieiot.com:8080/GSmart_PreProd/"; //Raspberry original D
    // public static  String URL_GENIE_AWS ="http://192.168.0.2:6060/";        //D local

    public static String URL_GENIE_AWS = "http://gsmarthome.genieiot.in";           //original
    public static String URL_GENIE = ""; //Raspberry

    //public static  String URL_GENIE_AWS ="http://192.168.0.2:6060";
    public static String URL_GENIE_LOCAL = "http://192.168.0.119:8080/smart_home_local";
  //  public static String URL_GENIE_AWS = "http://192.168.0.119:8080/smart_home_local";


    public static String GENIE_AWS = "192.168.0.2:6060";
    public static String messageType = null;

    public static final String MY_PREFS_NAME = "MyPrefsFile";
    static boolean flag = false;

    public static String TOPIC_INTERNET="refreshIntGenieHome";
    public static String TOPIC_LOCAL="refreshIntGenieHomeId_";
    String connectionValue;


    public static boolean isInternetAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }








    public static final MediaType JSON = MediaType.parse("application/json; charset=UTF8");
    OkHttpClient client = new OkHttpClient();




    public String doPostRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        client.setConnectTimeout(14, TimeUnit.SECONDS);  //Connect timeout




        client.setReadTimeout(14, TimeUnit.SECONDS);    //Socket timeout

        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doGetRequest(String url,String auth_token) throws IOException {
        Request request = new Request.Builder()
                .header("X-AUTH-TOKEN",auth_token)
                .url(url)
                .build();
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doPostRequest(String url, String json,String auth_token) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .header("X-AUTH-TOKEN",auth_token)
                .url(url)
                .post(body)
                .build();
        client.setConnectTimeout(14, TimeUnit.SECONDS);  //Connect timeout
        client.setReadTimeout(14, TimeUnit.SECONDS);    //Socket timeout
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doPostRoomRequest(String url, String json,String auth_token) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .header("X-AUTH-TOKEN",auth_token)
                .url(url)
                .post(body)
                .build();
        client.setConnectTimeout(64, TimeUnit.SECONDS);  //Connect timeout
        client.setReadTimeout(64, TimeUnit.SECONDS);    //Socket timeout
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doPostRoomNewRequest(String url, String json,String auth_token) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .header("X-AUTH-TOKEN",auth_token)
                .url(url)
                .post(body)
                .build();
        client.setConnectTimeout(8, TimeUnit.SECONDS);  //Connect timeout
        client.setReadTimeout(8, TimeUnit.SECONDS);    //Socket timeout
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }



    public String  chkStatus(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting ())
        {
            try {
                new AsyncTaskCheckRouter().execute().get();
            } catch (InterruptedException e) {
                Log.d("connection_checking","checked"+e.getMessage());
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d("connection_checking","checked"+e.getMessage());
                e.printStackTrace();
            }

            return  connectionValue;
        }
        else if (mobile.isConnectedOrConnecting ()) {

            connectionValue=INTERNET;
            return  connectionValue;
        } else {
            connectionValue=NOTINTERNET;

            return  connectionValue;
        }

    }


    private class AsyncTaskCheckRouter extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {

            InetAddress routername = null;

            Log.d("mqtt_checking","checked");
            try {
                routername = InetAddress.getByName("192.168.0.119");
                if (routername.isReachable(300)) //5000
                {
                    Log.d("network_checking12","Router is   reachable");
                    connectionValue = LOCAL_HUB;
                }
                else
                {
                    Log.d("network_checking12","Router is not  reachable");
                    connectionValue = INTERNET;
                }

            } catch (java.io.IOException e) {
                Log.d("network_checking12","Router EXCEPTION"+e.getMessage());
                e.printStackTrace();
            }

            return connectionValue;

        }


    }











}

